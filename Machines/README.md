# Machines

| Difficulty                   |
|------------------------------|
| [Easy](1.Easy/README.md)     |
| [Medium](2.Medium/README.md) |
| [Hard](3.Hard/README.md)     |
| [Insane](4.Insane/README.md) |
