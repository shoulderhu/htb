# Starting Point

| Tier                       | Machines | Comment                             |
|----------------------------|---------:|-------------------------------------|
| [Tier 0](Tier 0/README.md) | 8        | The key is a strong foundation      |
| [Tier 1](Tier 1/README.md) | 10       | You need to walk before you can run | 
| [Tier 2](Tier 2/README.md) | 7        | Don't forget to contemplate         | 
