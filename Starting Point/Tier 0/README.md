# Tier 0

| Machine                              | OS      | |
|--------------------------------------|---------|-|
| [Meow](Meow/README.md)               | Linux   | |
| [Fawn](Fawn/README.md)               | Linux   | |
| [Dancing](Dancing/README.md)         | Windows | |
| [Redeemer](Redeemer/README.md)       | Linux   | |
| [Explosion](Explosion/README.md)     | Windows | |
| [Preignition](Preignition/README.md) | Linux   | |
| [Mongod](Mongod/README.md)           | Linux   | |
| [Synced](Synced/README.md)           | Linux   | |