# Meow

<img src="61b5837dfdfe1fb1ca3750cf2712da44.png" width="300px">

## Task
1. What does the acronym VM stand for? 

2. What tool do we use to interact with the operating system in order to issue commands via the command line, such as the one to start our VPN connection? It's also known as a console or shell. 

3. What service do we use to form our VPN connection into HTB labs? 

4. What tool do we use to test our connection to the target with an ICMP echo request? 

5. What is the name of the most common tool for finding open ports on a target? 

6. What service do we identify on port 23/tcp during our scans? 

7. What username is able to log into the target over telnet with a blank password? 

## Flag
```python
import socket

def main():
    print("Hello World")


if __name__ == "__main__":
    pass
```

> [!NOTE]
> An alert of type 'note' using global style 'callout'.

{% codesnippet "/Starting Point/Tier 0/Meow/test.py" %}{% endcodesnippet %}

字段1 | 字段2 | 字段3
---|---|----
值1 | 值2abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567892abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890123456789 | 值3